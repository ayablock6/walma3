import axios from 'axios';

import envConfig from '../config/env.config.js';
const { myFatoorahUrl } = envConfig;

const callFatoorahAPI = async (url, dat, config) => {
    const { data } = await axios.post(
        `${myFatoorahUrl}${url}`,
        dat,
        config
    )

    return data;
}

export default { callFatoorahAPI };