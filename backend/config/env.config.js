import dotenv from 'dotenv'
const result = dotenv.config({ path: '.env' });
if (result.error) {
  throw result.error
}
const {
  NODE_ENV,
  APP_NAME,
  APP_PROTOCOL,
  APP_HOST,
  APP_PORT,
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_USERNAME,
  DB_PASSWORD,
  MY_FATOORAH_URL,
  MY_FATOORAH_ID,
  MY_FATOORAH_CALLBACK_URL,
  EMAIL_ADDRESS,
  EMAIL_PASSWORD,
  FROM_ADDRESS,
  MAX_FILE_SIZE,
} = process.env;

let envConfig = {};

if (NODE_ENV === 'production' || NODE_ENV === 'prod') {
  envConfig = {
    nodeEnv: NODE_ENV,
    appName: APP_NAME,
    appProtocol: APP_PROTOCOL,
    appHost: APP_HOST,
    appPort: APP_PORT,
    dbHost: DB_HOST,
    dbPort: DB_PORT,
    dbName: DB_NAME,
    dbUserName: DB_USERNAME,
    dbPassword: DB_PASSWORD,
    myFatoorahUrl: MY_FATOORAH_URL,
    myFatoorahId: MY_FATOORAH_ID,
    myFatoorahCallbackUrl: MY_FATOORAH_CALLBACK_URL,
    emailAddress: EMAIL_ADDRESS,
    emailPassword: EMAIL_PASSWORD,
    fromAddress: FROM_ADDRESS,
    maxFileSize: MAX_FILE_SIZE,
  };
} else if (NODE_ENV === 'staging' || NODE_ENV === 'stag') {
  envConfig = {
    nodeEnv: NODE_ENV,
    appName: APP_NAME,
    appProtocol: APP_PROTOCOL,
    appHost: APP_HOST,
    appPort: APP_PORT,
    dbHost: DB_HOST,
    dbPort: DB_PORT,
    dbName: DB_NAME,
    dbUserName: DB_USERNAME,
    dbPassword: DB_PASSWORD,
    myFatoorahUrl: MY_FATOORAH_URL,
    myFatoorahId: MY_FATOORAH_ID,
    myFatoorahCallbackUrl: MY_FATOORAH_CALLBACK_URL,
    emailAddress: EMAIL_ADDRESS,
    emailPassword: EMAIL_PASSWORD,
    fromAddress: FROM_ADDRESS,
    maxFileSize: MAX_FILE_SIZE,
  };
} else if (NODE_ENV === 'testing' || NODE_ENV === 'test') {
  envConfig = {
    nodeEnv: NODE_ENV,
    appName: APP_NAME,
    appProtocol: APP_PROTOCOL,
    appHost: APP_HOST,
    appPort: APP_PORT,
    dbHost: DB_HOST,
    dbPort: DB_PORT,
    dbName: DB_NAME,
    dbUserName: DB_USERNAME,
    dbPassword: DB_PASSWORD,
    myFatoorahUrl: MY_FATOORAH_URL,
    myFatoorahId: MY_FATOORAH_ID,
    myFatoorahCallbackUrl: MY_FATOORAH_CALLBACK_URL,
    emailAddress: EMAIL_ADDRESS,
    emailPassword: EMAIL_PASSWORD,
    fromAddress: FROM_ADDRESS,
    maxFileSize: MAX_FILE_SIZE,
  };
} else if (NODE_ENV === 'development' || NODE_ENV === 'dev') {
  envConfig = {
    nodeEnv: NODE_ENV,
    appName: APP_NAME,
    appProtocol: APP_PROTOCOL,
    appHost: APP_HOST,
    appPort: APP_PORT,
    dbHost: DB_HOST,
    dbPort: DB_PORT,
    dbName: DB_NAME,
    dbUserName: DB_USERNAME,
    dbPassword: DB_PASSWORD,
    myFatoorahUrl: MY_FATOORAH_URL,
    myFatoorahId: MY_FATOORAH_ID,
    myFatoorahCallbackUrl: MY_FATOORAH_CALLBACK_URL,
    emailAddress: EMAIL_ADDRESS,
    emailPassword: EMAIL_PASSWORD,
    fromAddress: FROM_ADDRESS,
    maxFileSize: MAX_FILE_SIZE,
  };
} else if (NODE_ENV === 'local') {
  envConfig = {
    nodeEnv: NODE_ENV,
    appName: APP_NAME,
    appProtocol: APP_PROTOCOL,
    appHost: APP_HOST,
    appPort: APP_PORT,
    dbHost: DB_HOST,
    dbPort: DB_PORT,
    dbName: DB_NAME,
    dbUserName: DB_USERNAME,
    dbPassword: DB_PASSWORD,
    myFatoorahUrl: MY_FATOORAH_URL,
    myFatoorahId: MY_FATOORAH_ID,
    myFatoorahCallbackUrl: MY_FATOORAH_CALLBACK_URL,
    emailAddress: EMAIL_ADDRESS,
    emailPassword: EMAIL_PASSWORD,
    fromAddress: FROM_ADDRESS,
    maxFileSize: MAX_FILE_SIZE,
  };
}

export default envConfig;
