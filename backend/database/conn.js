import mongoose from 'mongoose'
import colors from 'colors'

import envConfig from '../config/env.config.js';
const { dbHost, dbName, dbUserName, dbPassword } = envConfig;

const connectDB = async () => {
  try {
    const mongoUri = `mongodb+srv://${dbUserName}:${dbPassword}@${dbHost}/${dbName}?retryWrites=true&w=majority`;
    console.log(mongoUri);
    const conn = await mongoose.connect(mongoUri, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    });
    console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.underline);
  } catch (error) {
    console.log(`Error : ${error.message}`.red.underline.bold)
    process.exit(1)
  }
}

export default connectDB
