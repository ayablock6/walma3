import mongoose from 'mongoose'
import dotenv from 'dotenv'
import mongooseIntl from 'mongoose-intl'
import users from './data/users.js'
import products from './data/products.js'
import cities from './data/cities.js'
import City from '../../models/city.model.js'
import User from '../../models/user.model.js'
import Product from '../../models/product.model.js'
import Order from '../../models/order.model.js'
import connectDB from '../conn.js'

dotenv.config()

connectDB()
mongoose.plugin(mongooseIntl, {
  languages: ['en', 'ar'],
  defaultLanguage: 'en',
})

const importData = async () => {
  try {
    await Order.deleteMany()
    await City.deleteMany()
    await Product.deleteMany()
    await User.deleteMany()

    const createdUsers = await User.insertMany(users)
    const adminUser = createdUsers[0]._id

    const sampleProducts = products.map((product) => {
      return {
        ...product,

        user: adminUser,
      }
    })
    await Product.insertMany(sampleProducts)

    const createCities = await City.insertMany(cities)

    console.log('data imported'.green)
    process.exit()
  } catch (error) {
    console.error(`${error}`.red.inverse)
    process.exit(1)
  }
}

const destroyData = async () => {
  try {
    await City.deleteMany()
    await Order.deleteMany()
    await Product.deleteMany()
    await User.deleteMany()

    console.log('data destroyed'.red)
    process.exit()
  } catch (error) {
    console.error(`${error}`.red.inverse)
    process.exit(1)
  }
}

if (process.argv[2] === '-d') {
  destroyData()
} else {
  importData()
}
