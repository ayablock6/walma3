import asyncHandler from 'express-async-handler'
import City from '../models/city.model.js'

const getCityByName = asyncHandler(async (req, res) => {
  const city = await City.findOne({
    code: req.params.code,
  })
  res.json(city)
})

const setCityByShipping = asyncHandler(async (req, res) => {
  console.log(req.body)
  const { code, shippingPrice, cityName } = req.body
  const city = await City.findOne({
    code: code,
  })
  const objIndex = city.places.findIndex((obj) => obj.en === cityName)
  city.places[objIndex].shipping = shippingPrice
  console.log(objIndex)
  const updatedCity = await city.save()
  res.json(updatedCity)
})

export { getCityByName, setCityByShipping }
