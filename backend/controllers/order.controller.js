import path from 'path'

import asyncHandler from 'express-async-handler'
import nodemailer from 'nodemailer'
import sendgrid from '@sendgrid/mail'
import PDFDocument from 'pdfkit'
import Order from '../models/order.model.js'
import envConfig from '../config/env.config.js'
import commonServices from '../services/common.service.js'

import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const walmaLogo = path.join(__dirname, `../../assets/images/walma-logo.jpg`)

const { callFatoorahAPI } = commonServices
const {
  myFatoorahCallbackUrl,
  myFatoorahId,
  emailAddress,
  emailPassword,
  fromAddress,
} = envConfig

const addOrderItems = asyncHandler(async (req, res) => {
  const {
    customerName,
    orderItems,
    shippingAddress,
    paymentMethod,
    itemsPrice,
    taxPrice,
    shippingPrice,
    totalPrice,
  } = req.body.order

  if (orderItems && orderItems.length === 0) {
    res.status(400)
    throw new Error('No order items')
    return
  } else {
    const order = new Order({
      customerName,
      shippingAddress,
      orderItems,
      paymentMethod,
      itemsPrice,
      taxPrice,
      shippingPrice,
      totalPrice,
    })

    if (req.body.userInfo) {
      order.user = req.body.userInfo._id
    }
    const createdOrder = await order.save()
    res.status(201).json(createdOrder)
  }
})

const getOrderById = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id).populate(
    'user',
    'name email'
  )

  if (order) {
    res.json(order)
  } else {
    res.status(404)
    throw new Error('Order not found')
  }
})

const deleteOrder = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id)
  // req.params.id gets its value from the url

  if (order) {
    await order.remove()
    res.json({ message: 'order removed' })
  } else {
    res.status(404)
    throw new Error('order not found')
  }
})

const updateOrderToPaid = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id)
    .populate('orderItems.product')
    .populate('user', 'name email')

  if (order) {
    if (req.body.status === 'success') {
      order.orderItems.map((p) => {
        p.product.countInStock = p.product.countInStock - p.qty
        if (p.product.sale.status) {
          p.product.sale.qty = p.product.sale.qty - p.qty
        }
        p.product.save()
      })
      order.isPaid = true
      order.paidAt = Date.now()
      const doc = new PDFDocument()
      // const docImg = await doc.embedPng(fs.readFileSync(`/uploads/walma-logo.jpg`));
      doc.image(walmaLogo, {
        fit: [100, 100],
        align: 'center',
        valign: 'center',
      })

      doc
        .fontSize(16)
        .text(`Order Id                          : ${order._id}`, 50, 200)
      doc
        .fontSize(16)
        .text(
          `Customer name                     :  ${order.customerName}`,
          50,
          230
        )
      doc
        .fontSize(16)
        .text(
          `Customer phone number             : ${order.shippingAddress.phoneNumber}`,
          50,
          260
        )

      doc
        .fontSize(16)
        .text(
          `Customer Address       : ${order.shippingAddress.governorate}, ${order.shippingAddress.city}, ${order.shippingAddress.address}`,
          50,
          290
        )

      let y = 320
      order.orderItems.map((item) => {
        doc
          .fontSize(16)
          .text(
            `${item.qty}  ${item.product.name}       :  ${
              item.qty * item.price
            } KD `,
            50,
            y
          )
        y = y + 30
      })
      doc
        .fontSize(16)
        .text(`shipping Price               : ${order.shippingPrice} KD`, 50, y)
      y = y + 30
      doc
        .fontSize(16)
        .text(`Total Price                  : ${order.totalPrice} KD`, 50, y)

      doc.end()
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: emailAddress,
          pass: emailPassword,
        },
      })
      transporter.sendMail({
        from: fromAddress,
        to: fromAddress,
        subject: 'Walma Coffee',
        text: `You have new order`,
        attachments: [
          {
            filename: 'order.pdf',
            content: doc,
          },
        ],
      })
      if (order.user) {
        var mailOptions = {
          from: fromAddress,
          to: order.user.email,
          subject: 'Walma Coffee',
          text: `Your order has been paid successfully \n Thank You`,
          attachments: [
            {
              filename: 'order.pdf',
              content: doc,
            },
          ],
        }

        transporter.sendMail(mailOptions)
      }
    }

    order.paymentResult = {
      id: req.body.id,
      status: req.body.status,
      payment_id: req.body.paymentId,
    }

    const updatedOrder = await order.save()
    res.json(updatedOrder)
  } else {
    res.status(404)
    throw new Error('Order not found')
  }
})

const cashOrder = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id)
    .populate('orderItems.product')
    .populate('user', 'name email')

  order.orderItems.map((p) => {
    p.product.countInStock = p.product.countInStock - p.qty
    if (p.product.sale.status) {
      p.product.sale.qty = p.product.sale.qty - p.qty
    }
    p.product.save()
  })
  const doc = new PDFDocument()
  // const docImg = await doc.embedPng(fs.readFileSync(`/uploads/walma-logo.jpg`));
  doc.image(walmaLogo, {
    fit: [100, 100],
    align: 'center',
    valign: 'center',
  })

  doc
    .fontSize(16)
    .text(`Order Id                          : ${order._id}`, 50, 200)
  doc
    .fontSize(16)
    .text(`Customer name                     :  ${order.customerName}`, 50, 230)
  doc
    .fontSize(16)
    .text(
      `Customer phone number             : ${order.shippingAddress.phoneNumber}`,
      50,
      260
    )

  doc
    .fontSize(16)
    .text(
      `Customer Address       : ${order.shippingAddress.governorate}, ${order.shippingAddress.city}, ${order.shippingAddress.address}`,
      50,
      290
    )

  let y = 320
  order.orderItems.map((item) => {
    doc
      .fontSize(16)
      .text(
        `${item.qty}  ${item.product.name}  :  ${item.qty * item.price} KD `,
        50,
        y
      )
    y = y + 30
  })
  doc.fontSize(16).text(`shipping Price : ${order.shippingPrice} KD`, 50, y)
  y = y + 30
  doc.fontSize(16).text(`Total Price : ${order.totalPrice} KD`, 50, y)

  doc.end()
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: emailAddress,
      pass: emailPassword,
    },
  })
  transporter.sendMail({
    from: fromAddress,
    to: fromAddress,
    subject: 'Walma Coffee',
    text: `You have new order`,
    attachments: [
      {
        filename: 'order.pdf',
        content: doc,
      },
    ],
  })
  if (order.user) {
    var mailOptions = {
      from: fromAddress,
      to: order.user.email,
      subject: 'Walma Coffee',
      text: `Your order check \n Thank You`,
      attachments: [
        {
          filename: 'order.pdf',
          content: doc,
        },
      ],
    }

    transporter.sendMail(mailOptions)
  }
  res.json('new cash order')
})

const getMyOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({ user: req.user._id })
  res.json(orders)
})

const payMyOrders = asyncHandler(async (req, res) => {
  var executePaymentConfig = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${myFatoorahId}`,
    },
  }

  var data1 = JSON.stringify({
    InvoiceAmount: req.body.totalPrice,
    CurrencyIso: 'KWD',
  })

  var initiatePaymentConfig = {
    headers: {
      Authorization: `Bearer ${myFatoorahId}`,
      'Content-Type': 'application/json',
      Cookie:
        'ApplicationGatewayAffinityCORS=3ef0c0508ad415fb05a4ff3f87fb97da; ApplicationGatewayAffinity=3ef0c0508ad415fb05a4ff3f87fb97da',
    },
  }

  // var { data } = await axios.post(
  //   'https://api.myfatoorah.com/v2/InitiatePayment',
  //   data1,
  //   initiatePaymentConfig
  // )

  const data = await callFatoorahAPI(
    'InitiatePayment',
    data1,
    initiatePaymentConfig
  )

  const total = req.body.totalPrice

  var dat = JSON.stringify({
    PaymentMethodId: req.body.paymentMethod,
    CustomerName: 'name',
    NotificationOption: 'ALL',
    MobileCountryCode: '965',
    CustomerMobile: '12345678',
    CustomerEmail: 'mail@mail.com',
    InvoiceValue: total,
    DisplayCurrencyIso: 'kwd',
    CallBackUrl: `${myFatoorahCallbackUrl}${req.body._id}/success`,
    ErrorUrl: `${myFatoorahCallbackUrl}${req.body._id}/error`,
    Language: 'en',
    CustomerReference: 'noshipping-nosupplier',
    CustomerAddress: {
      Block: 'string',
      Street: 'string',
      HouseBuildingNo: 'string',
      Address: 'Address',
      AddressInstructions: 'Address',
    },

    InvoiceItems: [
      {
        ItemName: 'string',
        Quantity: 1,
        UnitPrice: total,
      },
    ],
    SourceInfo: 'string',
  })

  // const { data: data2 } = await axios.post(
  //   'https://api.myfatoorah.com/v2/ExecutePayment',
  //   dat,
  //   config
  // )
  // res.json(data2);

  const data2 = await callFatoorahAPI(
    'ExecutePayment',
    dat,
    executePaymentConfig
  )

  res.json(data2)
})

const getOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({}).populate('user', 'id name') //populate to get user and id associated with order
  res.json(orders)
})

const updateOrderToDelivered = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id)

  if (order) {
    order.isDeliverd = true
    order.deliverdAt = Date.now()

    const updatedOrder = await order.save()
    res.json(updatedOrder)
  } else {
    res.status(404)
    throw new Error('Order not found')
  }
})

export {
  addOrderItems,
  getOrderById,
  updateOrderToPaid,
  cashOrder,
  getMyOrders,
  getOrders,
  deleteOrder,
  payMyOrders,
  updateOrderToDelivered,
}
