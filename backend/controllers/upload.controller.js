import asyncHandler from 'express-async-handler'
const fileUpload = asyncHandler(async (req, res) =>{
    const fileName = `/uploads/${req.file.filename}`;
    res.status(200).send(fileName);
});

export {fileUpload};