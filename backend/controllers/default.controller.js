import asyncHandler from 'express-async-handler'

const getPong = asyncHandler(async (req, res) => {
  res.status(200).json({ data: "pong" })
})

export { getPong };
