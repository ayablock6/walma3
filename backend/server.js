import envConfig from './config/env.config.js'
import morgan from 'morgan'
import express from 'express'
import http from 'http'

import connectDB from './database/conn.js'
import { notFound, errorHandler } from './middlewares/error.middleware.js'
import mongoose from 'mongoose'
import mongooseIntl from 'mongoose-intl'
import path from 'path'
import walmaRoutes from './routes/index.js'

const { nodeEnv, appName, appProtocol, appHost, appPort } = envConfig

connectDB()
const app = express()

if (nodeEnv === 'development' || nodeEnv === 'local') {
  app.use(morgan('dev')) // only set for development mode
}

app.use(express.json())

app.use((req, res, next) => {
  next()
})
mongoose.plugin(mongooseIntl, {
  languages: ['en', 'ar'],
  defaultLanguage: 'en',
})

app.use(walmaRoutes)

const __dirname = path.resolve()
//this makes the uploads folder static. important for images.
app.use('/uploads', express.static(path.join(__dirname, '../uploads/')))
app.use('/assets', express.static(path.join(__dirname, '../assets/')))
// app.use(express.static(path.join(__dirname, '/../frontend/build')))
// app.get('*', (req, res) =>
//   res.sendFile(path.resolve(__dirname, '../frontend', 'build', 'index.html'))
// )
/*if (process.env.NODE_ENV === 'production') {
} else {
  app.get('/', (req, res) => {
    res.send('API is running')
  })
}*/

app.use(notFound)
app.use(errorHandler)

const server = http.createServer(app).listen(appPort, () => {
  console.log(
    `${appName} is listening on ${appProtocol}://${appHost}:${appPort}`
  )
})

const terminateProcess = (code) => {
  process.exit(code)
}

server.on('error', (err) => {
  const { code, syscall } = err
  console.log(code)
  if (syscall !== 'listen') {
    throw err
  }
  // handle specific listen errors with friendly messages
  if (code === 'EACCES') {
    console.error(`Port requires elevated privileges`)
    terminateProcess(1)
  }
  if (code === 'EADDRINUSE') {
    console.error(`Port is already in use`)
    terminateProcess(1)
  }
  if (code === 'ECONNREFUSED') {
    console.error(
      `No connection could be made because the target machine actively refused it.`
    )
    terminateProcess(1)
  }
  console.error('Error occured while booting the server')
  console.error(err)
  throw err
})

process
  .on('beforeExit', (code) => {
    console.log(`Process beforeExit event with code ${code}`)
  })
  .on('exit', (code) => {
    console.log(`Process exit event with code ${code}`)
  })
  .on('SIGINT', () => {
    /**
     * SIGTERM and SIGINT have default handlers on non-Windows platforms
     * that resets, the terminal mode before exiting with
     * code 128 + signal number.
     * If one of these signals has a listener installed, its default
     * behavior will be removed (Node.js will no longer exit).
     */
    if (server.listening) {
      shutdownServer(server, 0, 'SIGINT')
    }
  })
  .on('SIGTERM', () => {
    /**
     * SIGTERM is used to cause a program termination.
     * It is a way to politely ask a program to terminate.
     * The program can either handle this signal,
     * clean up resources and then exit,
     * or it can ignore the signal.
     */
    shutdownServer(server, 0, 'SIGTERM')
  })
  .on('SIGTSTP', () => {
    shutdownServer(server, 0, 'SIGTSTP')
  })
  .on('SIGTSTOP', () => {
    shutdownServer(server, 0, 'SIGTS')
  })
  .on('SIGHUP', () => {
    shutdownServer(server, 0, 'SIGHUP')
  })
  .on('SIGQUIT', () => {
    shutdownServer(server, 0, 'SIGQUIT')
  })
  .on('SIGABRT', () => {
    shutdownServer(server, 0, 'SIGABRT')
  })
  // Start reading from stdin so we don't exit.
  .on('unhandledRejection', (error, promise) => {
    // Emitted when promises are rejected and no handler is
    // attached to the promise.
    console.error(`Oops! forgot to handle a promise reject here, ${promise}`)
    console.error(`The error was: ${error}`)
    terminateProcess(1)
  })
  .on('uncaughtException', (error, origin) => {
    // Emitted when javascript error is not properly handeled.
    console.error('UncaughtExpection emitted')
    console.error(`Ohhooooo! Something terrible happend? ${origin}`)
    console.error(`The error was: ${error}`)
    // console.error(process.stderr.fd, origin, error);
    terminateProcess(1)
  })
  .on('warning', (warning) => {
    logger.warn(warning.name)
    logger.warn(warning.message)
    logger.warn(warning.stack)
  })

function shutdownServer(server, code, signal) {
  if (server) {
    console.log(`Received signal [${signal}] to gracefully shutdown a server`)
    // reject the new connection or stop to receive a new
    // TCP packet and close the server.
    server.close(() => {
      console.log(`Gracefully shutdown server with code ${code}`)
      terminateProcess(code)
    })

    // If server hasn't finished in 500 ms, let's shutdown process
    setTimeout(() => {
      console.log(
        'All remaining pending unclosed connections should shutdown forcefully.'
      )
      terminateProcess(0)
    }, 500).unref() // Don't register timeout on event loop
  }
}

console.log('hi')
