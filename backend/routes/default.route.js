import express from 'express'
import { getPong } from '../controllers/default.controller.js'

const router = express.Router()

router.route('/ping').get(getPong)

export default router
