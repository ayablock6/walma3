import express from 'express'
import { protect, admin } from '../middlewares/auth.middleware.js'
const router = express.Router()

import {
  getCode,
  deleteCode,
  createCode,
  getCodesList,
} from '../controllers/promocode.controller.js'

router.route('/:code').get(getCode).delete(protect, admin, deleteCode)
router.route('/').get(getCodesList).post(protect, admin, createCode)

export default router
