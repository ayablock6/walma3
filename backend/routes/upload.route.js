import express from 'express'
import path from 'path'
import multer from 'multer'
import {
  fileUpload  
} from '../controllers/upload.controller.js';
import envConfig from '../config/env.config.js';

const { maxFileSize } = envConfig;

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(path.resolve(), "../uploads/"))
  }, 
  filename: (req, file, cb) => {
    cb(null,  `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`)
  },
});

const fileFilter = (req, file, cb) =>{
  const extension = path.extname(file.originalname).toLowerCase();
  const mimetype = file.mimetype;
  if (
      extension === '.jpg' ||
      extension === '.jpeg' ||
      extension === '.png' ||
      mimetype === 'image/png' ||
      mimetype === 'image/jpg' ||
      mimetype === 'image/jpeg'
  ) {
    return cb(null, true);
  }else{
    cb(null, false);
    return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
  }
};

const uploadStorage = multer({ 
  storage: storage,   
  fileFilter: fileFilter,
  limits: {fileSize: parseInt(maxFileSize)},
});

const router = express.Router()
router.post('/', uploadStorage.single("image"), fileUpload)

export default router;
