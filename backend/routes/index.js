
import express from 'express'
const app = express();

import defaultRoutes from './default.route.js'
import productRoutes from './product.route.js'
import cityRoutes from './city.route.js'
import uploadRoutes from './upload.route.js'
import userRoutes from './user.route.js'
import promocodeRoute from './promoCodes.route.js'
import orderRoutes from './order.route.js'

app.use('/', defaultRoutes);
app.use('/api/products', productRoutes);
app.use('/api/city', cityRoutes)
app.use('/api/promocodes', promocodeRoute)
app.use('/api/upload', uploadRoutes)
app.use('/api/users', userRoutes)
app.use('/api/orders', orderRoutes);

export default app;