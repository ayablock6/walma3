import express from 'express'
import {
  addOrderItems,
  getOrderById,
  updateOrderToPaid,
  getMyOrders,
  getOrders,
  cashOrder,
  deleteOrder,
  payMyOrders,
  updateOrderToDelivered,
} from '../controllers/order.controller.js'
import { protect, admin } from '../middlewares/auth.middleware.js'

const router = express.Router()

router.route('/').post(addOrderItems).get(protect, admin, getOrders)
router.route('/myorders').get(protect, getMyOrders)
router.route('/payid').post(payMyOrders)
router.route('/:id').get(getOrderById).delete(protect, admin, deleteOrder)
router.route('/:id/cashorder').post(cashOrder)
router.route('/:id/pay').put(updateOrderToPaid)
router.route('/:id/deliver').put(protect, admin, updateOrderToDelivered)

export default router
