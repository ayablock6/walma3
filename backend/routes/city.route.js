import express from "express";
import {
  getCityByName,
  setCityByShipping,
} from "../controllers/city.controller.js";
import { protect, admin } from "../middlewares/auth.middleware.js";

const router = express.Router();

router.route("/:code").get(getCityByName);
router.route("/shipping").put(protect, admin, setCityByShipping);

export default router;
