module.exports = {
  "apps": [
    {
      "name": "walma:local",
      "script": "node server.js",
      "watch": true,
      "ignore_watch": [
        ".history",
        ".vscode",
        "logs",
        ".tmp",
        "node_modules"
      ],
      "instances": "1",
      "source_map_support": true,
      "exec_mode": "fork_mode",
      "env": {
        "NODE_ENV": "development"
      }
    }
  ]
}
