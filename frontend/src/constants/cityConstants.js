export const CITY_DETAILS_REQUEST = 'CITY_DETAILS_REQUEST'
export const CITY_DETAILS_FAIL = 'CITY_DETAILS_FAIL'
export const CITY_DETAILS_SUCCESS = 'CITY_DETAILS_SUCCESS'

export const CITY_UPDATE_REQUEST = 'CITY_UPDATE_REQUEST'
export const CITY_UPDATE_FAIL = 'CITY_UPDATE_FAIL'
export const CITY_UPDATE_SUCCESS = 'CITY_UPDATE_SUCCESS'
export const CITY_UPDATE_RESET = 'CITY_UPDATE_RESET'
